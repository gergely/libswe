all:
	cd src && ${MAKE}

install:
	cd src && ${MAKE} install
	cd doc && ${MAKE} install
	cd data && ${MAKE} install
